import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class PrefixApiInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
        const basedUrl = `http://localhost:3004`;

        const apiReq = req.clone({ url: `${basedUrl}${req.url}` });
    return next.handle(apiReq);
  }
}
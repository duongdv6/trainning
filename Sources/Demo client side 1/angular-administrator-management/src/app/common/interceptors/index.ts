/* "Barrel" of Http Interceptors */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PrefixApiInterceptor } from './api-prefix.interceptors/prefix-api.interceptors';


/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: PrefixApiInterceptor, multi: true },
];
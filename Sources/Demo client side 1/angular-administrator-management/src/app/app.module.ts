import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import {StoreModule} from '@ngrx/store';
import {reducer} from './reducers/customers.reducer'
import { CreateCustomerComponent } from './customers/create-customer/create-customer.component';
import { CustomersListComponent } from './customers/create-customer/customers-list.component';
import { CustomerDetailsComponent } from './customers/create-customer/customer-details.component';
import { SharedModule } from './shared-module/shared-module.module';
import { httpInterceptorProviders } from './common/interceptors';
import { CoreModule } from './common/core.module';
import { LoginComponent } from './common/authentication/login/login.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    CustomersListComponent,
    CreateCustomerComponent,
    CustomerDetailsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true, useHash: true } // <-- debugging purposes only
    ),

    CoreModule,
    SharedModule.forRoot(),
    StoreModule.forRoot({
      customer: reducer
    })
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from './app.state';
import { LoadCustomer } from './customers/actions/customers.action';
import { Customer } from './customers/models/customer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-administrator-management';
  description = 'NgRx Example';

  constructor(private http: HttpClient, private store: Store<AppState>){
    this.http.get<Customer[]>('/customers').subscribe(res => {
      this.store.dispatch(new LoadCustomer(res));
    });
  }
}
